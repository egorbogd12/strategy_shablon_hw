using UnityEngine;
using UnityEngine.UI;

public class PlayerReputation : MonoBehaviour
{
    [SerializeField] private int _startReputation;
    [SerializeField] private Button _addReputationButton;
    [SerializeField] private Button _decreaseReputationButton;
    [SerializeField] private Button _showReputationButton;

    private int _valueOfReputation = 50;
    public int _reputation { get; private set; }

    private void Awake()
    {
        _reputation = _startReputation;
        _addReputationButton.onClick.AddListener(AddReputation);
        _decreaseReputationButton.onClick.AddListener(DecreaseReputation);
        _showReputationButton.onClick.AddListener(ShowReputation);
    }

    private void AddReputation()
    {
        _reputation += _valueOfReputation;
        Debug.Log("�������� ���������: " + _valueOfReputation);
    }

    private void ShowReputation()
    {
        Debug.Log("���� ��������� �����: " + _reputation);
    }

    private void DecreaseReputation()
    {
        if (_reputation - _valueOfReputation >= 0)
        {
            _reputation -= _valueOfReputation;
            Debug.Log("������ ���������: " + _valueOfReputation);
        }
        else
        {
            Debug.Log("��������� �������� �� �������");
        }
    }
}
