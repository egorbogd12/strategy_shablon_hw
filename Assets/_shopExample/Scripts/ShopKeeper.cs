using UnityEngine;

public class ShopKeeper : MonoBehaviour
{
    [SerializeField] private int _reputationForTradeFruits;
    [SerializeField] private int _reputationForTradeArmor;

    private Itraderbehavior _traderbehavior;


    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out PlayerReputation reputation))
        {
            if (reputation._reputation < _reputationForTradeFruits)
            {
                SetTraderBehavior(new NoTradeBehavior());
                Trade();
            }

            else if (reputation._reputation >= _reputationForTradeFruits && reputation._reputation < _reputationForTradeArmor)
            {
                SetTraderBehavior(new TradeFruitsBehavior());
                Trade();
            }

            else if (reputation._reputation >= _reputationForTradeArmor)
            {
                SetTraderBehavior(new TradeArmorBehavior());
                Trade();
            }
        }
    }

    private void SetTraderBehavior(Itraderbehavior traderbehavior)
    {
        _traderbehavior = traderbehavior;
    }

    private void Trade()
    {
        _traderbehavior.Trade();
    }
}
