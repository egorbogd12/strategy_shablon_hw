using UnityEngine;
using System;

public abstract class GunBase : MonoBehaviour
{
    [SerializeField, Range(1, 10)] protected int _startAmmo;
    [SerializeField] protected Transform _bulletStartPosition;
    [SerializeField] protected GameObject _bulletprefab;

    protected float _bulletSpeed = 15f;
    protected int _currentAmmo;
    protected bool _isGunRemoved = false;
    protected IShoot _iShootType;

    private void Awake()
    {
        _currentAmmo = _startAmmo;
    }

    private void Update()
    {
        if (_isGunRemoved == false)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                TryShoot();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                Reload();
            }
        }
    }

    public void SetShootType(IShoot shootType)
    {
        _iShootType = shootType;
    }

    public void RemoveGun()
    {
        _isGunRemoved = true;
    }

    public void ActivateGun()
    {
        _isGunRemoved = false;
    }


    public void Reload()
    {
        _currentAmmo = _startAmmo;
        Debug.Log("������������� ...");
    }

    public void BulletSpawn()
    {
        GameObject bullet = Instantiate(_bulletprefab, _bulletStartPosition.position, Quaternion.identity);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        rb.velocity = transform.forward * _bulletSpeed;
    }    

    protected void Shoot()
    {
        int ammountOfBullets = _iShootType.GetBulletsForShot();
        switch (ammountOfBullets)
        {
            case 1:
                _iShootType.Shoot();
                BulletSpawn();
                break;
            case 3:
                _iShootType.Shoot();
                BulletSpawn();
                BulletSpawn();
                BulletSpawn();
                break;
        }
    }

    protected void TryShoot()
    {
        if (_currentAmmo - _iShootType.GetBulletsForShot() >= 0)
        {
            Shoot();
            UpdateBullets();
        }
        else
        {
            Debug.Log("��� �������� , ������������ !");
        }
    }

    protected void UpdateBullets()
    {
        _currentAmmo -= _iShootType.GetBulletsForShot();
        Debug.Log("�������� ��������: " + _currentAmmo);
       // return _currentAmmo;
    }



    //protected abstract void TryShoot();
    //protected abstract int UpdateBullets();


}
