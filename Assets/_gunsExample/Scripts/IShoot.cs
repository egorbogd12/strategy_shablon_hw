using UnityEngine;

public interface IShoot
{
    public void Shoot();

    public int GetBulletsForShot();
}
