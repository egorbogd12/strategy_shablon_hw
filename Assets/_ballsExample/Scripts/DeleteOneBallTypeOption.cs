using System.Collections.Generic;
using UnityEngine;

public class DeleteOneBallTypeOption : IWinOption
{
    private List<BallBase> _typeOfBallsToDelete;

    public DeleteOneBallTypeOption(List<BallBase> ballsToDeleteList)
    {
        _typeOfBallsToDelete = ballsToDeleteList;
    }

    public void Win()
    {
        if (_typeOfBallsToDelete.Count == 0)
        {
            Debug.Log("��� ������ ���� ����������, �� �������!");
        }
    }
}
