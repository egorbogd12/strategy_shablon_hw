using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] private GameManager _gameManager;
    [SerializeField] private ClickDestroyer _clickDestroyer;

    private void Awake()
    {
        _clickDestroyer.Initialize(_gameManager);
    }
}
