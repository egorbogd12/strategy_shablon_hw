using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [SerializeField] public List<BallBase> _AllBallsList;
    [SerializeField] public List<BallBase> _whiteBallsList;
    [SerializeField] public List<BallBase> _redBallsList;
    [SerializeField] public List<BallBase> _greenBallsList;
    [Space]
    [Header("������ ������ ������ ����")]
    [SerializeField] private Button _buttonSetDeleteAllWinOption;
    [SerializeField] private Button _buttonSetDeleteGreenBallsOption;
    [SerializeField] private Button _buttonSetDeleteRedBallsOption;
    [SerializeField] private Button _buttonSetDeleteWhiteBallsOption;

    private bool _gameStarted = false;
    private IWinOption _winOption;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
            Instance = this;

        _buttonSetDeleteAllWinOption.onClick.AddListener(SetDeleteAllWinOption);
        _buttonSetDeleteGreenBallsOption.onClick.AddListener(SetDeleteGreenBallsOption);
        _buttonSetDeleteRedBallsOption.onClick.AddListener(SetDeleteRedBallsOption);
        _buttonSetDeleteWhiteBallsOption.onClick.AddListener(SetDeleteWhiteBallsOption);

        SetWinOption(new DeleteOneBallTypeOption(_whiteBallsList));
    }

    public bool IsGameStarted()
    {
        return _gameStarted;
    }

    private void Update()
    {
        Win();
    }

    public IWinOption GetWinOption()
    {
        return _winOption;
    }

    private void SetWinOption(IWinOption winOption)
    {
        _winOption = winOption;
    }

    private void Win()
    {
        _winOption.Win();
    }

    private void SetDeleteAllWinOption()
    {
        SetWinOption(new DeleteAllBallsOption());
        _gameStarted = true;
    }

    private void SetDeleteGreenBallsOption()
    {
        SetWinOption(new DeleteOneBallTypeOption(_greenBallsList));
        _gameStarted = true;
    }

    private void SetDeleteRedBallsOption()
    {
        SetWinOption(new DeleteOneBallTypeOption(_redBallsList));
        _gameStarted = true;
    }

    private void SetDeleteWhiteBallsOption()
    {
        SetWinOption(new DeleteOneBallTypeOption(_whiteBallsList));
        _gameStarted = true;
    }
}
