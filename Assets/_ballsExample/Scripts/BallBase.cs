using System.Collections.Generic;
using UnityEngine;

public abstract class BallBase : MonoBehaviour
{
    public void DeleteFromList(List<BallBase> ballList)
    {
        ballList.Remove(this);
    }
}
