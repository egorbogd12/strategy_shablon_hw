using UnityEngine;

public class ClickDestroyer : MonoBehaviour
{
    private GameManager _gameManager;

    public void Initialize( GameManager gameManager)
    {
        _gameManager = gameManager;
    }

    private void Update()
    {
        if (_gameManager.IsGameStarted()) // ���� ����� ���� �� ������ ������ ������ ������
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.TryGetComponent(out BallBase ball))
                    {
                        if (ball.TryGetComponent(out WhiteBall whiteBall))
                        {
                            whiteBall.DeleteFromList(_gameManager._whiteBallsList);
                            Destroy(hit.collider.gameObject);
                        }

                        else if (ball.TryGetComponent(out RedBall redBall))
                        {
                            redBall.DeleteFromList(_gameManager._redBallsList);
                            Destroy(hit.collider.gameObject);
                        }

                        else if (ball.TryGetComponent(out GreenBall greenBall))
                        {
                            greenBall.DeleteFromList(_gameManager._greenBallsList);
                            Destroy(hit.collider.gameObject);
                        }
                        ball.DeleteFromList(_gameManager._AllBallsList);
                    }
                }
            }
        }
    }
}
